import React from 'react';
import "./styles/Message.css"


class Message extends React.Component {
    likeButton = (e)=>{
        const block = e.target.parentNode;
        if(block.getAttribute('fill') === 'red') {
            block.setAttribute('fill', 'white');
            block.classList.remove('message-liked');
            block.classList.add('message-like');
        }else{
            block.setAttribute('fill', 'red');
            block.classList.remove('message-like');
            block.classList.add('message-liked');
        }
    }
    shouldComponentUpdate(nextProps) {
        if(nextProps.name === this.props.name) {
            return false;
        }else {
            return true;
        }
    }
    render() {
        const time = new Date();
        let hours = time.getHours();
        let minutes = time.getMinutes();
        const msgTime = `${hours}:${minutes}`;
        return(
            <div className="message">
                <div className="message-user-avatar"><img width="110" height="92" src={this.props.photo} alt="avatar"/></div>
                <div className="message-user-name">{this.props.name}</div>
                <p className="message-text">{this.props.msg}</p>
                <time className="message-time">{msgTime}</time>
                
                <svg    onClick={this.likeButton}
                            xmlns="http://www.w3.org/2000/svg"
                            stroke="white"
                            fill="white"
                            width="16"
                            height="16"
                            className="message-like"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
            </div>
        )
    }
}

export default Message;