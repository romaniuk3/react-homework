import React from 'react';
import './styles/Header.css';

class Header extends React.Component {
    render() {

        return (
            <header className="header">
                <ul>
                    <li className="header-title">My chat</li>
                    <li className="header-users-count">17 participants</li>
                    <li className="header-messages-count">{this.props.messagesCount} messages</li>
                </ul>
                <div className="header-last-message-date">
                    Last message at {this.props.lastTime ? this.props.lastTime : '1 minute ago'}
                </div>
            </header>
        )
    }
}

export default Header;