import React from 'react';
import Message from './Message';
import './styles/MessageList.css'

class MessageList extends React.Component {
    state = {
        show: true,
        counter: 1,

    }

    render() {
        const data = this.props.data;

        return(
            <div className="message-list">

                <Message name = {data[0].user} msg = {data[0].text} photo = {data[0].avatar}/>
                <Message name = {data[1].user} msg = {data[1].text} photo = {data[1].avatar}/>
                <div className="messages-divider">Today</div>

                {this.props.messages}
                

            </div>
            
        )
    }
}

export default MessageList;