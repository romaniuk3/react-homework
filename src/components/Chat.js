import React from 'react';
import Preloader from './Preloader';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';



class Chat extends React.Component {
      state = {
            error: null,
            isLoaded: false,
            items: [],
            messages: [],
            time: ''
        }

    componentDidMount() {
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }

    createMessage = (value) => {
      const newMessage = value;
      let newarr = this.state.messages.concat(newMessage);
      this.setState(() => ({
        messages: newarr,
        time: value.props.time
      }));
    }

    
    render() {
        const { error, isLoaded, items } = this.state;
        if(error) {
            return <div>Erorr: {error.message}</div>
        }else if (!isLoaded) {
            return <Preloader />;
            
        }else {
            return(
                <div className="chat">
                  <Header lastTime = {this.state.time} messagesCount = {this.state.messages.length+2}/> 
                  <MessageList messages = {this.state.messages} data = {items}/>
                  <MessageInput onSend = {this.createMessage} data = {items}/>       
                    <footer style={{height: '100px'}}></footer>           
                </div>
                
            )   
        }
     
    }
    
}

export default Chat;