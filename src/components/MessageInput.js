import React from 'react';
import './styles/MessageInput.css';
import OwnMessage from './OwnMessage';

class MessageInput extends React.Component {
    state = {
        message: ''
     }
    sendMessage = () => { 
        const time = new Date();
        let hours = time.getHours();
        let minutes = time.getMinutes();
        const msgTime = `${hours}:${minutes}`;
        const inputText = document.querySelector('.message-input-text');
        this.setState({
            message: inputText.value
        },()=>{this.props.onSend(<OwnMessage time={msgTime} message={this.state.message}/>);})
        inputText.value = '';
    }
    render() {

        return(
            <div className="message-input">
                <input type="text" className="message-input-text" placeholder="Message"></input>
                <button onClick={this.sendMessage} className="message-input-button">Send</button>
                <button className="message-input-button save-hide">Save</button>
            </div>
        )
    }
}

export default MessageInput;