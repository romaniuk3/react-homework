import React from 'react';
import './styles/OwnMessage.css';

class OwnMessage extends React.Component {
    editMessage = (e) => {
        const msgContainer = e.target.parentNode.querySelector('.message-text');
        const savebtn = document.querySelector('.save-hide');
        const inputblock = document.querySelector('.message-input-text');
        inputblock.value = this.props.message;
        savebtn.style = 'display: inline';

        savebtn.addEventListener('click', ()=>{
            const inputmsg = document.querySelector('.message-input-text');
            savebtn.style = 'display: none';
            msgContainer.textContent = inputmsg.value;
            inputmsg.value = '';
        })
    }

    deleteMessage = (e) => {
        e.target.parentNode.remove();
    }
    
    render() {
        document.querySelector('.messages-divider').style="display: block";

            return(
                <div className="own-message">
                    <p className="message-text">{this.props.message}</p>
                    <button onClick={this.editMessage} className="message-edit">Edit</button>
                    <button onClick={this.deleteMessage} className="message-delete">Delete</button>
                    <time className="message-time">{this.props.time}</time>
                </div>
            )
        }
    }

export default OwnMessage;